# FileMakerPro data extraction tool

# Table of contents <!-- omit in toc -->

- [FileMakerPro data extraction tool](#filemakerpro-data-extraction-tool)
- [Details](#details)
  - [Licence](#licence)
- [How to build and run/install the application](#how-to-build-and-runinstall-the-application)
  - [Build the project](#build-the-project)
    - [On a Mac/Unix machine](#on-a-macunix-machine)
    - [On a Windows machine](#on-a-windows-machine)
  - [Export the database](#export-the-database)
    - [Run on a Macintosh](#run-on-a-macintosh)
      - [Installation of Java and FileMakerPro Java Database Driver](#installation-of-java-and-filemakerpro-java-database-driver)
      - [Allowing Java to Connect to FileMaker Pro](#allowing-java-to-connect-to-filemaker-pro)
      - [How to Run the Export Application and Connect to the FileMaker Pro Database](#how-to-run-the-export-application-and-connect-to-the-filemaker-pro-database)
    - [Run on a Windows machine](#run-on-a-windows-machine)

# Details

This has only been tested with v19 of FileMakerPro running on Mac.

## Licence

Copyright (C) 2021  _Noel Faux  - University of Melbourne_ 

_This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version._

_This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the 
GNU General Public License for more details._

_You should have received a copy of the GNU General Public License (`COPYING.txt`)
along with this program.  If not, see <https://www.gnu.org/licenses/>._

# How to build and run/install the application
## Build the project

- Ensure you have [maven](https://maven.apache.org) installed on your computer.
- Ensure you have as a minimum, [Java JDK 16](https://www.oracle.com/java/technologies/javase/jdk16-archive-downloads.html) installed on your computer.

### On a Mac/Unix machine

1. Open Terminal
    1. Clone the repository into your desired location (directory/folder/path)

        ```sh
        ~ $ cd your_desired_location
        your_desired_location $ git clone https://gitlab.unimelb.edu.au/mdap-public/fmproexport.git
        ```

    2. Type the following commands:

        ```sh
        your_desired_location $ cd FMPexport
        FMPexport $ mvn clean compile javafx:jlink
        ```

    3. This should download all required packaged/libaries, compile the code and generate a new directory `target`, with the following structure:

        ```ascii
        target/
        ├── classes/
        ├── generated-sources/
        ├── maven-status/
        ├── fmpexport.zip
        └── fmpexport/
            ├──  conf/
            ├── legal/
            ├── lib/
            └── bin/
                ├── fmpexport
                ├── java
                ├── jrunscript
                └── keytool
        ```

2. To run the application double-click on the `fmpexport` file in the `target/fmpexport/bin` directory.

__NOTE:__ To share the final package, share the `fmpexport.zip` file. It contains all necessary files to share for another user to unpack and run the `./bin/fmpexport` file.

### On a Windows machine

__*TO BE WRITTEN*__

## Export the database

### Run on a Macintosh

#### Installation of the FileMakerPro Java Database Driver

1. Download the FileMakerPro JDBC driver - [Download link](https://support.claris.com/s/article/Software-Update-FileMaker-xDBC-client-drivers-for-FileMaker-1503692806454?language=en_US)
   1. Click on the Mac button for the FileMaker Pro 19.3.1
   2. This is a .dmg file which when opened contains a folder called  "JDBC Client Driver", and inside the folder is the file "fmjbdc.jar"
   3. Copy this file onto your computer, in a location that you will remember. Maybe where you hold your database files

#### Allowing Java to Connect to FileMaker Pro

- Launch FileMaker Pro and open the database you desire to export from
  - __NOTE:__ make sure the database file name only contains a `.` before `fmp12`, e.g. `myFMPDatabase.fmp12` not `myFMPDATABASE.1.2.fmp12`. If so replace the `.` with `-` or `_` so you can keep the version number, i.e. `myFMPDATABASE_1-2.fmp12`
  - To enable access to the database, some security settings need to adjusted
    - Click on the "File" menu and select the following menu options `Manage -> Security`
      1. This will open a dialog panel; choose the bottom-left button `Advanced Settings...`
      2. This opens another dialog panel, with the `Privilege Sets` tab selected
      3. Select the `Privilege Set` that you belong to, such as `[Full Access]`
      4. Click the `Edit...` button
      5. This opens another dialog panel, ensure the `Access via ODBC/JDBC (fmxdbc)` is checked, i.e. tick in the check box to the left
      6. For each of the opened dialog boxes, click `OK`
      7. Click on the "File" menu and select the following menu options `Sharing -> Enable ODBC/JDBC` and ensure under the `ODBC/JDBC Settings` that `ODBC/JDBC Sharing` is `On`
  
#### How to Run the Export Application and Connect to the FileMaker Pro Database

There are two ways to launch the application:

1. Open the `bin` folder and double click on the file `fmpexport`
    1. Depending on your Mac OS security settings, this may fail
        - If so right click on the file and select `open` from the context menu
2. Or run via the command line, assuming you are in the `fmpexport` folder

    ```sh
    ~/fmpexport $ cd bin
    ~/fmpexport/bin $ ./fmpexport
    ```

3. The GUI:
      ![FMP export GUI](./imgs/FMPexportGUI.png)

4. To connect to the opened FileMaker Pro database:
   1. Click the `Choose file` button under the label "Please choose which database to export/extract" and select the database file you have open in FileMaker Pro
   2. Enter the "User Name" and "Password" of the user you wish to connect to the database with. That user must belong in the `Privilege Set` group above that you enabled `JDBC/ODBC` access to
   3. Click the `Choose file` button under the label "Please select the database driver (e.g. fmjdbc.jar) file"
       - Navigate to the location where you moved the `fmjdbc.jar` to, above, and select the file
   4. Click `Connect`
   5. If the connection is successful, then in the middle column `Choose which table(s)`, a list of available tables will appear, and in the `Output` two messages should be displayed:
      1. "Driver loaded: com.filemaker.jdbc.Driver@<driver_code>", where <driver_code> is a random alphanumeric
      2. "We have a connection to com.filemaker.jdbc3.J3.Connection@<driver_code>"

5. Now you are able to select all or some of the tables to export and choose a format: `csv` or `json`
  - If `csv` is chosen, each table is exported separately, as there is no clean way to export all tables in one big `csv`
  - If `json` is selected, then you have the option to choose to save the table exports as a `Single file` or as `Separate files for each table`

6. Click `Submit` to begin the export

7. If the export is successful, a message will be displayed in the `Output`
   - "Exported all the tables to \<saved location\>"

### Run on a Windows machine

__*TO BE WRITTEN*__

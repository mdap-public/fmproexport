module fmpexport {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.naming;

    opens uom.mdap to javafx.fxml;
    exports uom.mdap;
}
package uom.mdap;

import java.io.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Factory class allowing all tuples in a ResultSet, from a sql call to a FileMakerPro database, to be written to a file
 * as a well formed csv file.
 *
 * @author Noel Faux
 */
public class FMPcsvExport {

    /**
     * Class constructor.
     */
    public FMPcsvExport(){ }

    /**
     * Writes each tuple in the ResultSet, to the FileWriter, where each field is separated by a comma. Fields which need to be
     * quoted due to the presence of comma(s), they are quoted with the defined quote.
     *
     * @param fw The file Writer to write each tuple to.
     * @param records The ResultSet containing all the tuples to write to file.
     * @param fields An array containing HashMaps of the field names for each record in the records.
     *               The HashMap key:value is "Name":"Field Name".
     * @param quote The String use to quote elements in the record, if it contains a comma.
     * @param tableName The name of the table the ResultSet is derived from. Used in the output string,
     *                  in the Output TextField of the main GUI.
     * @param cont The Controller of the main GUI, which the output is written to.
     */
    public void write_rows(Writer fw, ResultSet records, ArrayList<HashMap<String, String>> fields,
                           String quote, String tableName, Controller cont){
        int count = 0;
        try{
            while(records.next()) {
                count++;
                for (HashMap<String, String> field : fields) {
                    String fname = field.get("Name");
                    String value = records.getString(fname);
                    if (value == null) {
                        value = "";
                    }
                    if (value.contains(",")) {
                        fw.write(quote + value + quote + ",");
                    } else {
                        fw.write(value + ",");
                    }
                }
                fw.write("\n");
            }
            cont.updateStatus("Table: " + tableName + " contained: " + count + " records\n");
            fw.close();
        } catch (Exception err){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            err.printStackTrace(pw);
            System.exit(2);
        }
    }

    /**
     * Writes each tuple in the ResultSet, to the FileWriter, where each field is separated by a comma.
     * Fields which need to be quoted due to the presence of comma(s), they are quoted with a default character, ".
     *
     * @param fw The file Writer to write each tuple to.
     * @param records The ResultSet containing all the tuples to write to file.
     * @param fields An array containing HashMaps of the field names for each record in the records.
     *        The HashMap key:value is "Name":"Field Name".
     * @param tableName The name of the table the ResultSet is derived from. Used in the output string,
     *                  in the Output TextField of the main GUI.
     * @param cont The Controller of the main GUI, which the output is written to.
     */
    public void write_rows(Writer fw, ResultSet records, ArrayList<HashMap<String, String>> fields,
                           String tableName, Controller cont){
        this.write_rows(fw, records, fields, "\"", tableName, cont);
    }

    /**
     * Writes the header names, separated by a comma, to the file Writer. These are just the field names in the table the
     * ResultSet is from.
     *
     * @param fwriter The file Writer to write csv header to.
     * @param frs The ResultSet containing the field names for a given table in a FileMakerPro Database.
     * @param cont The Controller of the main GUI, which the output is written to.
     * @return An ArrayList of HasMaps. Each HashMap contains the key:value, "Name":"Field Name"
     */
    public ArrayList<HashMap<String, String>> writeHeader(Writer fwriter, ResultSet frs, Controller cont) {
        ArrayList<HashMap<String, String>> fields = new ArrayList<HashMap<String, String>>();
        try {
            while (frs.next()) {
                String fname = frs.getString("FieldName");
                HashMap<String, String> field = new HashMap<String, String>(2);
                field.put("Name", fname);
                fields.add(field);
                cont.updateStatus("Field: " + field + "\n");
                fwriter.write(fname);
                if(!frs.isLast()){
                    fwriter.write(",");
                }
            }
            fwriter.write("\n");
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }

        return fields;
    }
}

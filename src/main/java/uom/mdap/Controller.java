package uom.mdap;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

/**
 * The main Controller for the GUI allowing the user to connect to a FileMarkerPro Database, select all or some tables
 * and export the data as either a csv, or JSON formatted file(s). If csv format is chosen, then each table is written
 * out in separate files. JSON formatted export can either be written as a single file or a single file for each
 * selected table.
 */
public class Controller {

    @FXML private RadioButton rbSingle;
    @FXML private RadioButton rbSplit;
    @FXML private CheckBox selectAllCb;
    @FXML private Button loginButton;
    @FXML private ListView<CheckBox> tableList;
    @FXML private Button submitButton;
    @FXML private TextField userNameField;
    @FXML private PasswordField passwordField;

    @FXML private ComboBox saveFormatCB;
    @FXML private ToggleGroup manyORone;

    @FXML private TextField dbDriverFile;
    @FXML private TextField dbLocation;
    @FXML private TextField saveLocation;
    @FXML private TextArea outPutArea;

    @FXML private TextArea sqlStatement;
    @FXML private TextArea sqlOutput;
    @FXML private Button submitSQLbutton;

    private String saveDir = "";
    private String DB_URL = "";
    private Driver dbDriver = null;
    private Connection conn = null;

    /**
     * Returns the username used to access the FileMakerPro database the user entered.
     *
     * @return The username of the account to access the FileMakerPro database.
     */
    private String getUserName(){
        return userNameField.getText();
    }

    /**
     * Returns the password used to access the FileMakerPro database the user entered.
     *
     * @return The password of the account to access the FileMakerPro database.
     */
    private String getPassword(){
        return passwordField.getText();
    }

    /**
     * Returns the data export format chosen by the user, csv or json.
     *
     * @return Either csv or json.
     */
    private String getSaveType(){ return saveFormatCB.getValue().toString(); }

    /**
     * Are we writing each table out in a separate file or as a single file.
     *
     * @return The text of the radio button selected in the toggle group.
     */
    private String getNfiles(){ return ((RadioButton)manyORone.getSelectedToggle()).getText(); }

    /**
     *  Creates an array containing the names of the tables in the FileMarkerPro database, which has been chosen
     *  by the user for export, based on the selection status of the associated checkbox.
     *
     * @param tablesList List of checkboxes whose associated text is a table name, from the FileMarkerPro database.
     *                   This is populated once the database has been connected.
     * @return An array of selected table names.
     */
    private ArrayList<String> getSelectedTables(ListView<CheckBox> tablesList){
        ArrayList<String> sTablesList = new ArrayList<String>();
        for (CheckBox cb : tablesList.getItems()){
            if(cb.isSelected()){
                sTablesList.add(cb.getText());
            }
        }
        return sTablesList;
    }

    /**
     * Displays the text message in the Output TextArea on the GUI.
     *
     * @param message The text to be displayed to the user in the Output TextArea.
     */
    public void updateStatus(String message) {
        if (Platform.isFxApplicationThread()) {
            outPutArea.appendText(message);
        } else {
            Platform.runLater(() -> outPutArea.appendText(message));
        }
    }

    /**
     * Displays the text message in the sqlOutput TextArea.
     *
     * @param message The text to be displayed to the user in the sqlOutput TextArea.
     */
    public void updateSQLOutPut(String message){
        if (Platform.isFxApplicationThread()) {
            sqlOutput.appendText(message);
        } else {
            Platform.runLater(() -> sqlOutput.appendText(message));
        }
    }

    /**
     * This connects to the FileMakerPro database and for each selected table in the table list, collects the field
     * names and types for the table, then collects all the records (tuples) in the selected table. Then writes the
     * records out, in the chosen format (csv, json), to file.
     *
     * @param un The username to connect to the FileMarkerPro database with.
     * @param pssw The password to connect to the FileMarkerPro database with.
     */
    public void exportTables(String un, String pssw){
        try{
            Writer fwriter = null;
            String format = this.getSaveType();
            String singleOrSplit = this.getNfiles();
            FMPcsvExport csvExport = new FMPcsvExport();
            FMPjsonExport jsonExport = new FMPjsonExport();
            boolean singleF = singleOrSplit.equals("Single file");
            // Unable to write all tables to a single file as a csv formatted file
            if(format.equals("csv")){
                singleF = false;
            }

            this.updateStatus("Saving as a " + format + "\n");
            this.updateStatus("Saving in " + singleOrSplit + " files" + "\n");

            if( this.conn == null) {
                this.conn = DriverManager.getConnection(DB_URL, un, pssw);
                this.updateStatus("We have a connection");
                this.updateStatus(this.conn.toString() + "\n");
            }
            Statement stmt = this.conn.createStatement();
            Statement recordStmt = this.conn.createStatement();

            String fieldsSql = "SELECT * FROM FileMaker_Fields WHERE TableName = ?";
            PreparedStatement fieldStmt = this.conn.prepareStatement(fieldsSql);

            if (singleF && format.equals("json")){
                fwriter = new FileWriter(Paths.get(this.saveDir, "Full_Export-" + this.dbLocation.getText() +
                        "-" + java.time.LocalDate.now() + ".json").toString());
                jsonExport.writeTables(fwriter, this);
            }

            ListIterator<String> tableNamesIter = this.getSelectedTables(this.tableList).listIterator();
            while(tableNamesIter.hasNext()){
                String tableName = tableNamesIter.next();
                if(!singleF) {
                    if (format.equals("json")) {
                        fwriter = new FileWriter(Paths.get(this.saveDir, tableName + "-" +
                                this.dbLocation.getText() + "-" + java.time.LocalDate.now() + ".json").toString());
                    } else if (format.equals("csv")){
                        fwriter = new FileWriter(Paths.get(this.saveDir, tableName + "-" +
                                this.dbLocation.getText() + "-" + java.time.LocalDate.now() + ".csv").toString());
                    }
                }
                // Getting the fields for the table
                this.updateStatus("=".repeat(10) + "\nTable: " + tableName+"\n");

                if(format.equals("json")){
                    jsonExport.writeTable(fwriter, tableName, singleF, this);
                }

                // Build list of fields, so we don't need to execute for each record
                fieldStmt.setString(1, tableName);

                ResultSet frs = fieldStmt.executeQuery();

                ArrayList<HashMap<String, String>> fields = new ArrayList<HashMap<String, String>>();

                if(format.equals("json")){
                    fields = jsonExport.writeFields(fwriter, frs, this);
                } else if (format.equals("csv")){
                    fields = csvExport.writeHeader(fwriter, frs, this);
                }

                // Getting the records/tuples in the table
                if(recordStmt.execute("SELECT * FROM \""+tableName+"\"")){
                    ResultSet records = recordStmt.getResultSet();
                    if(format.equals("json")){
                        jsonExport.writeTuples(fwriter, records, tableName, fields, this);
                    } else if(format.equals("csv")){
                        csvExport.write_rows(fwriter, records, fields, tableName, this);
                    }
                } else {
                    this.updateStatus(tableName + " Did not contain eny records"+"\n");
                }

                if(format.equals("json")){
                    jsonExport.writeTableClose(fwriter, singleF, !tableNamesIter.hasNext(), this);
                }
           } // Tables
           if(format.equals("json") && singleF){
                jsonExport.writeTablesClose(fwriter, this);
           }
        } catch(Exception e){
            this.updateStatus(e.toString());
        }
        this.updateStatus("Exported all the tables to " + this.saveDir + "\n");
    }

    /**
     * Collects a list of tables present in the FileMakerPro database and then displays them table name linked
     * to a CheckBox.
     *
     * @param conn Database connection to the FileMakerPro Database.
     */
    public void listTables(Connection conn){
        try {
            ObservableList<CheckBox> tList =  FXCollections.observableArrayList();
            Statement stmt = this.conn.createStatement();
            String query = "SELECT TableName FROM FileMaker_Tables";
            if(stmt.execute(query)) {
                ResultSet tables = stmt.getResultSet();
                while(tables.next()) {
                    String tableName = tables.getString("TableName");
                    CheckBox cb = new CheckBox(tableName);
                    cb.selectedProperty().addListener(new ChangeListener<Boolean>(){
                        public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                            int scbCount = 0;
                            int ts = 0;
                            for (CheckBox lcb : tableList.getItems()){
                                ts++;
                                if (lcb.isSelected()){
                                    scbCount++;
                                }
                            }
                            boolean allCb = scbCount == ts;
                            selectAllCb.setSelected(allCb);
                        }
                    });
                    cb.setSelected(true);
                    tList.add(cb);
                }
                this.tableList.setItems(tList);
                this.selectAllCb.setDisable(false);
                this.selectAllCb.setSelected(true);
            }
        } catch (SQLException sqlException){
            this.updateStatus(sqlException.toString());
        }
    }

    /**
     * Updated the CheckBoxes in the table's list, based on the status of the 'All Tables' CheckBox. If selected all
     * CheckBoxes in the list are selected, otherwise they are deselected.
     *
     * @param actionEvent The action trigger associated with the 'All Tables' CheckBox.
     */
    public void handleSelectAllTables(ActionEvent actionEvent) {
        CheckBox cb = (CheckBox)actionEvent.getSource();
        if(cb.isSelected()){
            for (CheckBox lcb : this.tableList.getItems()) {
                lcb.setSelected(true);
            }
        } else {
            for (CheckBox lcb : this.tableList.getItems()) {
                lcb.setSelected(false);
            }
        }
    }

    /**
     * Opens the systems file chooser, allowing the user to choose the FileMarkerPro database file to connect to.
     *
     * @param actionEvent The action trigger associated with the 'Choose file' button, under the text
     *                    'Please choose which database to export/extract'.
     */
    public void handleChooseDb(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setTitle("Select the database to access...");
        File selectedDb = fileChooser.showOpenDialog((Stage) node.getScene().getWindow());
        String dbFile = selectedDb.getName();
        String dbName = dbFile.substring(0, dbFile.lastIndexOf(".fmp"));
        this.dbLocation.setText(dbName);
        this.DB_URL = "jdbc:filemaker://localhost/" + dbName;

        if(!this.saveDir.equals("") | this.dbDriver != null) {
            this.submitButton.setDisable(false);
        }

        if(this.dbDriver != null){
            this.updateStatus("Driver has been registered: " + this.dbDriver.toString() + "\n");
            this.loginButton.setDisable(false);
            // Comment this out if the SQL GUI has been uncommented.
            // this.submitSQLbutton.setDisable(false);
        }
    }

    /**
     * Creates a connection to the chosen FileMakerPro database, on click of the 'Connect' button.
     *
     * @param actionEvent The action trigger associated with the 'Connect' button.
     */
    @FXML private void handleLoginButtonAction(ActionEvent actionEvent) {
        try {
            Properties ps = new Properties();
            ps.put("user", this.getUserName());
            ps.put("password", this.getPassword());
            this.conn = this.dbDriver.connect(this.DB_URL, ps);
            this.updateStatus("We have a connection to: ");
            this.updateStatus(this.conn.toString() + "\n");
            this.listTables(this.conn);
        } catch(SQLException sqlException) {
            this.updateStatus("\n" + sqlException.toString() + "\n");
        }
    }

    /**
     * Opens the systems file chooser, allowing the user to choose the folder/directory to save the export file(s) to.
     *
     * @param actionEvent The action trigger associated with the 'Choose location' button.
     */
    @FXML private void handleSaveLoc(ActionEvent actionEvent) {
        Node node = (Node) actionEvent.getSource();
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        dirChooser.setTitle("Select the directory to save the export to...");
        File chosenDir = dirChooser.showDialog((Stage) node.getScene().getWindow());
        this.saveDir = chosenDir.getAbsolutePath();
        this.saveLocation.setText(this.saveDir);

        if(!this.DB_URL.equals("") | this.dbDriver != null) {
            this.submitButton.setDisable(false);
        }
    }

    /**
     * Starts the database export process the user has defined.
     *
     * @param e The action trigger associated with the 'Submit' button.
     */
    @FXML private void handleSubmitButtonAction(ActionEvent e){
        Thread thread = new Thread(() -> {
            outPutArea.setText("Starting to export from the database\n");
            this.exportTables(this.getUserName(), this.getPassword());
        });
        thread.start();
    }

    /**
     * Quits the application.
     *
     * @param actionEvent The action trigger associated with the 'Exit' button.
     */
    @FXML private void handleExitButtonAction(ActionEvent actionEvent) {
        outPutArea.appendText("Bye");
        System.exit(0);
    }

    /**
     * Executes the sql statement in the sqlStatement TextArea
     *
     * @param actionEvent The action trigger associated with the 'Submit SQL' button.
     */
    @FXML private void handleSubmitSQLButtonAction(ActionEvent actionEvent) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, this.getUserName(), this.getPassword());
            this.updateStatus("We have a connection");
            this.updateStatus(conn.toString() + "\n");
            Statement stmt = conn.createStatement();
            String query = this.sqlStatement.getText();
            if(stmt.execute(query)) {
                this.sqlOutput.clear();
                ResultSet res = stmt.getResultSet();
                ResultSetMetaData resMeta = res.getMetaData();
                for (int i = 1; i <= resMeta.getColumnCount(); i++){
                    this.updateSQLOutPut(resMeta.getColumnName(i) + " | ");
                }
                this.updateSQLOutPut("\n");
                while(res.next()) {
                    for (int i = 1; i <= resMeta.getColumnCount(); i++){
                        this.updateSQLOutPut(res.getString(i) + " | ");
                    }
                    this.updateSQLOutPut("\n");
                }
            }
        } catch (SQLException err) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            err.printStackTrace(pw);
            this.updateSQLOutPut(sw.toString());
        }
    }

    /**
     * Opens the systems file chooser, allowing the user to choose the jdbc driver, the application needs to use, to
     * connect to the chosen FileMakerPro database.
     *
     * @param ae The action trigger associated with the 'Choose file' button, under the text 'Please select the database
     *           driver (e.g. fmpjdbc.jar) file'.
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @FXML private void handleChooseDbDriver(ActionEvent ae) throws MalformedURLException, ClassNotFoundException,
            NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Node node = (Node) ae.getSource();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.setTitle("Select the database diver (eg. fmpjdbc.jar).");
        File selectedDbDriver = fileChooser.showOpenDialog((Stage) node.getScene().getWindow());
        this.dbDriverFile.setText(selectedDbDriver.getAbsolutePath());
        URL dbJarUrl = new URL("jar:file:" + this.dbDriverFile.getText() + "!/");
        String className = "com.filemaker.jdbc.Driver";
        URLClassLoader ucl = new URLClassLoader(new URL[] { dbJarUrl });
        this.dbDriver = (Driver)Class.forName(className, true, ucl).getDeclaredConstructor().newInstance();


        this.updateStatus("Driver loaded: " + this.dbDriver.toString() + "\n");

        if(!this.saveDir.equals("") | !this.DB_URL.equals("")) {
            this.submitButton.setDisable(false);
        }

        if(!this.DB_URL.equals("")){
            this.loginButton.setDisable(false);
            // Comment this out if the SQL GUI has been uncommented.
            // this.submitSQLbutton.setDisable(false);
        }
    }

    /**
     * Enables and disables the Save as: 'Single file' radio buttons depending on the format chosen to save the export to.
     * If csv, then 'Single file' is disabled and 'Separate files for each table' is selected and disabled. Both of these are enabled if 'json' is the
     * selected format.
     *
     * @param actionEvent The action trigger associated with the format chooser, ComboBox.
     */
    @FXML private void handleSaveFormat(ActionEvent actionEvent) {
        if(this.getSaveType().equals("csv")){
            this.rbSplit.setSelected(true);
            this.rbSplit.setDisable(true);
            this.rbSingle.setDisable(true);
        } else {
            this.rbSplit.setDisable(false);
            this.rbSingle.setDisable(false);
        }
    }
}

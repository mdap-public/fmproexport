package uom.mdap;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Factory class allowing all tuples in a ResultSet, from a sql call to a FileMakerPro database, to be written to a file
 * as a well formed json file.
 *
 * @author Noel Faux
 * @see <a href="https://gitlab.unimelb.edu.au/mdap-public/fmproexport/-/blob/master/README.md">JSON definition</a>
 */
public class FMPjsonExport {
    private int indent;

    /**
     * Class constructor.
     */
    FMPjsonExport(){
        this.indent = 0;
    }

    /**
     * Writes the initial opening JSON elements for a collection of tables, to the file Writer.
     *
     * @param fwriter The file Writer to write to.
     * @param cont The Controller of the main GUI, which the errors are written to.
     */
    public void writeTables(Writer fwriter, Controller cont){
        try{
            this.indent++;
            fwriter.write("{\n\t\"tables\": [\n");
            this.indent++;
        } catch (IOException ioe){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
    }

    /**
     * Writes the closing JSON elements for a collection of tables
     *
     * @param fwriter The file Writer to write to.
     * @param cont The Controller of the main GUI, which the errors are written to.
     */
    public void writeTablesClose(Writer fwriter, Controller cont){
        try{
            fwriter.write("\t]\n}\n");
            this.indent--;
            fwriter.close();
        } catch (IOException ioe){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
    }

    /**
     * Writes the opening JSON element for a table.
     *
     * @param fwriter The file Writer to write to.
     * @param tableName The name of the table being written.
     * @param singleF A flag to indicate if this table to being written to a separate file. If true, then the opening
     *                element is different compared to if it is part of a set table and written to a single file.
     * @param cont The Controller of the main GUI, which the errors are written to.
     */
    public void writeTable(Writer fwriter, String tableName, boolean singleF, Controller cont){
        try {
            if (!singleF) {
                fwriter.write("{\n");
                this.indent++;
            }
            fwriter.write("\t".repeat(this.indent) + "\"table\":{\n");
            this.indent++;
            fwriter.write("\t".repeat(this.indent) + "\"name\":\"" + tableName + "\",\n");
        } catch (IOException ioe){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
    }

    /**
     * Writes the closing JSON element of a table.
     *
     * @param fwriter The file Writer to write to.
     * @param singleF A flag to indicate if this table to being written to a separate file. If true, then the closing
     *                element is different compared to if it is part of a set table and written to a single file.
     * @param lastTable A flag to indicate if the table is the last of a set of tables.
     * @param cont The Controller of the main GUI, which the errors are written to.
     */
    public void writeTableClose(Writer fwriter, boolean singleF, boolean lastTable, Controller cont){
        try{
            fwriter.write("\t".repeat(this.indent) + "}");
            if(!singleF){
                this.indent--;
                fwriter.write("\t".repeat(this.indent)+"}");// End of a table
                fwriter.close();
            } else {
                if (!lastTable) {
                    fwriter.write(",\n");
                } else {
                    fwriter.write("\n");
                }
            }
        }catch (IOException ioe){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ioe.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
    }

    /**
     * Writes out the field definitions for a table.
     *
     * @param fwriter The file Writer to write to.
     * @param frs The ResultsSet containing the field names and type from a table, from the FileMakerPro database.
     * @param cont The Controller of the main GUI, which the output is written to.
     * @return
     */
    public ArrayList<HashMap<String, String>> writeFields(Writer fwriter, ResultSet frs, Controller cont){

        ArrayList<HashMap<String, String>> fields = new ArrayList<HashMap<String, String>>();
        try {
            fwriter.write("\t".repeat(this.indent) + "\"fields\":[\n");
            this.indent++;
            while (frs.next()) {
                String fname = frs.getString("FieldName");
                String ftype = frs.getString("FieldType");
                HashMap<String, String> field = new HashMap<String, String>(2);
                field.put("Name", fname);
                fields.add(field);
                cont.updateStatus("Field: " + field + "\n");
                fwriter.write("\t".repeat(this.indent) + "\"field\"{\n");
                this.indent++;
                fwriter.write("\t".repeat(this.indent) + "\"name\":\"" + fname + "\",\n");
                fwriter.write("\t".repeat(this.indent) + "\"type\":\"" + ftype + "\"\n");
                this.indent--;
                fwriter.write("\t".repeat(this.indent) + "}\n");
            }
            this.indent--;
            fwriter.write("\t".repeat(this.indent) + "],\n");
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            cont.updateStatus("Closing table error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
        return fields;
    }

    /**
     * Writes all the tuples of a table to file.
     *
     * @param fwriter The file Writer to write to.
     * @param records The records (tuples) to write to file
     * @param tableName Name of the table the records belong to. This is only use for user feedback.
     * @param fields An array of HashMaps containing the field names.
     *               Each HashMap contains the key value pair, "Name": "Field Name".
     * @param cont The Controller of the main GUI, which the output is written to.
     */
    public void writeTuples(Writer fwriter, ResultSet records, String tableName,
                           ArrayList<HashMap<String, String>> fields, Controller cont){
        int count = 0;
        try{
            fwriter.write("\t".repeat(this.indent) + "\"tuples\":[\n");
            this.indent++;
            while(records.next()) {
                fwriter.write("\t".repeat(this.indent) + "\"tuple\":{\n");
                this.indent++;
                count++;
                for(int i = 0; i < fields.size(); i++){
                    HashMap<String, String> tfield = fields.get(i);
                    String fname = tfield.get("Name");
                    String value = records.getString(fname);
                    if(value == null){
                        value = "";
                    }
                    fwriter.write("\t".repeat(this.indent) + "\""+ fname +"\":\"" +
                            value.replace("\"", "\\\"") + "\"");
                    if (i+1 != fields.size()) {
                        fwriter.write(",\n");
                    }
                }
                this.indent--;
                fwriter.write("\n");
                fwriter.write("\t".repeat(indent) + "}"); // End of a tuple
                if (!records.isLast()) {
                    fwriter.write(",\n");
                }
            }
            this.indent--;
            cont.updateStatus("Table: " + tableName + " contained: " + count + " records\n");
            if(count != 0) {
                fwriter.write("\n");
            }
            fwriter.write("\t".repeat(this.indent) + "]\n"); // End of the tuples
            this.indent--;
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            cont.updateStatus("Tables error:\n:" + sw.toString() + "\n");
            System.exit(2);
        }
    }
}
